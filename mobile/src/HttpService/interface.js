var openurl = {
  getLoginCaptcha:"sys/captcha/getLoginCaptcha",
  login:"sys/member/login",
  sign:"sys/member/sign",
  logout:"sys/member/logout",
  getAllSysConfig:"sys/config/getAllSysConfig",
  resetMemberPwd:"sys/member/resetMemberPwd",
  getInformationById:"sys/information/getInformationById",
  getAllMobileClientBanner:"sys/config/getAllMobileClientBanner",
}

var memberAssetUrl = {
  getMyAsset:"sys/member/asset/getMyAsset",
  findAllMyAssetLog:"sys/member/asset/findAllMyAssetLog",
  getAllMyBanks:"sys/member/asset/getAllMyBanks",
  findAllMyRechargeLog:"sys/member/asset/findAllMyRechargeLog",
  withdrawBalance:"sys/member/asset/withdrawBalance",
  addMemberBank:"sys/member/asset/addMemberBank",
  modifyMemberBank:"sys/member/asset/modifyMemberBank",
}

var memberUrl ={
  getMemberUserBaseInfoVo:"sys/member/getMemberUserBaseInfoVo",
  modifyMemberPwd:"sys/member/modifyMemberPwd",
  getMemberInviteCode:"sys/member/getMemberInviteCode",
  getAllMemberRecommend:"sys/member/getAllMemberRecommend",
  modifyMemberUserBaseInfo:"sys/member/modifyMemberUserBaseInfo",
  findAllSysInformation:"sys/member/findAllSysInformation",
  getTodayCheckIn:"sys/member/getTodayCheckIn",
  memberCheckIn:"sys/member/memberCheckIn",
  findAllCheckInReward:"sys/member/findAllCheckInReward",
}
var memberbetUrl = {
}
var fileUrl ={
  uploadSingleHeadImage:"sys/member/uploadSingleHeadImage",
}

var URLS={};
var interfaces={openurl,memberAssetUrl,memberUrl,memberbetUrl,fileUrl};

for(var k in interfaces){
  for(var j in interfaces[''+k+'']){
    URLS[''+j+'']=interfaces[''+k+''][''+j+''];
  }
}
console.log(URLS);

export default URLS;
