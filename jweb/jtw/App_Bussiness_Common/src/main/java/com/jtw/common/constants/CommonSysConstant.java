package com.jtw.common.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 * DESCRIPT: 系统级常量定
 *
 * @author cjsky666
 * @date 2018/11/19 11:54
 */
public class CommonSysConstant {
    /**
     * 当前操作系统文件路径格式,并没有什么卵用，
     * 全部写成“/”就可以了。反正windows上“\”或者“/”都认识。linux上只认识“/”
     */
    public static final String KEY_FILE_PATH_SEPARATOR= "/";
}
