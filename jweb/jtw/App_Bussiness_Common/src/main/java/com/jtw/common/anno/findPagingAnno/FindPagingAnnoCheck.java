package com.jtw.common.anno.findPagingAnno;

import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.FindParamVo;
import com.jtw.common.util.PagingUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * DESCRIPT: 筛选条件的控制器校验。若参数非法直接过滤掉
 *
 * @author cjsky666
 * @date 2018/11/5 13:49
 */
@Aspect
@Component
@Slf4j
public class FindPagingAnnoCheck {
    @Before("@annotation(findPagingAnno)")
    public void checkFindPagingVo(JoinPoint joinPoint, FindPagingAnno findPagingAnno) {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        // 记录下请求内容
        log.debug("URL : " + request.getRequestURL().toString());
        log.debug("HTTP_METHOD : " + request.getMethod());
        log.debug("IP : " + request.getRemoteAddr());
        log.debug("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.debug("ARGS : " + Arrays.toString(joinPoint.getArgs()));
        log.debug("columns :" + findPagingAnno.items());

        FindPagingItemAnno[] pagingItemAnno = findPagingAnno.items();
        //比对当前的筛选条件
        FindPagingVo findPagingVo = (FindPagingVo) joinPoint.getArgs()[0];

        List<FindParamVo> findParamVos = findPagingVo.getFindParams();
        if (findParamVos != null) {
            log.info(findParamVos.toString());
            for (FindParamVo findParamVo : findParamVos) {
                PagingUtil.FilterParams(findParamVo, pagingItemAnno);
            }
        }
    }
}
