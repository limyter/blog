package com.jtw.common.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by keliii on 2018/7/20.
 */
public class ImageCaptchaUtil {

//      /**
     //     * 验证码字体
     //     */
//    private static final Font mFont = new Font("Times New Roman", Font.ROMAN_BASELINE, 20);
//
//    /**
//     * 验证码宽度
//     */
//    private static final int IMG_WIDTH = 80;
//
//    /**
//     * 验证码高度
//     */
//    private static final int IMG_HEIGTH = 20;
//
//    public static String build(HttpServletResponse response) {
//        response.setHeader("Pragma", "No-cache");
//        response.setHeader("Cache-Control", "no-cache");
//        response.setDateHeader("Expires", 0);
//        response.setContentType("image/jpeg");
//        BufferedImage image = new BufferedImage(IMG_WIDTH, IMG_HEIGTH, BufferedImage.TYPE_INT_RGB);
//        Graphics g = image.getGraphics();
//        Random random = new Random();
//        g.setColor(getRandColor(200, 250));
//        g.fillRect(1, 1, IMG_WIDTH - 1, IMG_HEIGTH - 1);
//        g.setColor(new Color(102, 102, 102));
//        g.drawRect(0, 0, IMG_WIDTH - 1, IMG_HEIGTH - 1);
//        g.setColor(getRandColor(160, 200));
//        for (int i = 0; i < 80; i++) {
//            int x = random.nextInt(IMG_WIDTH - 1);
//            int y = random.nextInt(IMG_HEIGTH - 1);
//            int xl = random.nextInt(6) + 1;
//            int yl = random.nextInt(12) + 1;
//            g.drawLine(x, y, x + xl, y + yl);
//        }
//        g.setColor(getRandColor(160, 200));
//        for (int i = 0; i < 80; i++) {
//            int x = random.nextInt(IMG_WIDTH - 1);
//            int y = random.nextInt(IMG_HEIGTH - 1);
//            int xl = random.nextInt(12) + 1;
//            int yl = random.nextInt(6) + 1;
//            g.drawLine(x, y, x - xl, y - yl);
//        }
//        g.setFont(mFont);
//        String sRand = "";
//        for (int i = 0; i < 4; i++) {
//            String tmp = getRandomChar();
//            sRand += tmp;
//            g.setColor(new Color(20 + random.nextInt(110), 20 + random
//                    .nextInt(110), 20 + random.nextInt(110)));
//            g.drawString(tmp, 15 * i + 10, 15);
//        }
//
//        g.dispose();
//        try {
//            ImageIO.write(image, "JPEG", response.getOutputStream());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return sRand;
//    }
//
//    /**
//     * 随机获取颜色
//     * @param fc
//     * @param bc
//     * @return Color
//     */
//    private static Color getRandColor(int fc, int bc) {
//        Random random = new Random();
//        if (fc > 255){
//            fc = 255;
//        }
//        if (bc > 255){
//            bc = 255;
//        }
//        int r = fc + random.nextInt(bc - fc);
//        int g = fc + random.nextInt(bc - fc);
//        int b = fc + random.nextInt(bc - fc);
//        return new Color(r, g, b);
//    }
//
//    /**
//     * 随机生成字符：大写字母、小写字母或数字
//     *
//     * @return String
//     */
//    private static String getRandomChar() {
//        int rand = (int) Math.round(Math.random() * 2);
//        long itmp = 0;
//        char ctmp = '\u0000';
//        switch (rand) {
//            case 1:
//                itmp = Math.round(Math.random() * 25 + 65);
//                ctmp = (char) itmp;
//                return String.valueOf(ctmp);
//			/*case 2:
//				itmp = Math.round(Math.random() * 25 + 97);
//				ctmp = (char) itmp;
//				return String.valueOf(ctmp);*/
//            default:
//                itmp = Math.round(Math.random() * 9);
//                return String.valueOf(itmp);
//        }
//
//    }

    //使用到Algerian字体，系统里没有的话需要安装字体，字体只显示大写，去掉了1,0,i,o几个容易混淆的字符
    public static final String VERIFY_CODES = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    public static final String VERIFY_NUMBER_CODES = "1234567890";
    private static Random random = new Random();


    /**
     * 使用系统默认字符源生成验证码
     * @param verifySize    验证码长度
     * @return
     */
    public static String generateVerifyCode(int verifySize){
        return generateVerifyCode(verifySize, VERIFY_NUMBER_CODES);
    }
    /**
     * 使用指定源生成验证码
     * @param verifySize    验证码长度
     * @param sources   验证码字符源
     * @return
     */
    public static String generateVerifyCode(int verifySize, String sources){
        if(sources == null || sources.length() == 0){
            sources = VERIFY_CODES;
        }
        int codesLen = sources.length();
        Random rand = new Random(System.currentTimeMillis());
        StringBuilder verifyCode = new StringBuilder(verifySize);
        for(int i = 0; i < verifySize; i++){
            //随机在验证字符中取一个值
            verifyCode.append(sources.charAt(rand.nextInt(codesLen-1)));
        }
        return verifyCode.toString();//得到四位数的验证码
    }

    /**
     * 生成随机验证码文件,并返回验证码值
     * @param w
     * @param h
     * @param outputFile
     * @param verifySize
     * @return
     * @throws IOException
     */
    public static String outputVerifyImage(int w, int h, File outputFile, int verifySize) throws IOException{
        String verifyCode = generateVerifyCode(verifySize);
        outputImage(w, h, outputFile, verifyCode);
        return verifyCode;
    }

    /**
     * 输出随机验证码图片流,并返回验证码值
     * @param w
     * @param h
     * @param os
     * @param verifySize
     * @return
     * @throws IOException
     */
    public static String outputVerifyImage(int w, int h, OutputStream os, int verifySize) throws IOException {
        String verifyCode = generateVerifyCode(verifySize);
        outputImage(w, h, os, verifyCode);
        return verifyCode;
    }

    /**
     * 生成指定验证码图像文件
     * @param w
     * @param h
     * @param outputFile
     * @param code
     * @throws IOException
     */
    public static void outputImage(int w, int h, File outputFile, String code) throws IOException{
        if(outputFile == null){
            return;
        }
        File dir = outputFile.getParentFile();//文件目录
        if(!dir.exists()){
            dir.mkdirs();
        }
        try{
            outputFile.createNewFile();//创建文件
            FileOutputStream fos = new FileOutputStream(outputFile);
            outputImage(w, h, fos, code);
            fos.close();
        } catch(IOException e){
            throw e;
        }
    }

    /**
     * 输出指定验证码图片流
     * @param w  宽
     * @param h   高
     * @param os   输出流方式
     * @param code   验证码
     * @throws IOException
     */
    public static void outputImage(int w, int h, OutputStream os, String code) throws IOException{
        int verifySize = code.length();
        //类型为预定义图像类型之一的 BufferedImage。
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Random rand = new Random();
        Graphics2D g2 = image.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        Color[] colors = new Color[5];
        Color[] colorSpaces = new Color[] { Color.WHITE, Color.CYAN,
                Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE,
                Color.PINK, Color.YELLOW };
        float[] fractions = new float[colors.length];
        for(int i = 0; i < colors.length; i++){
            colors[i] = colorSpaces[rand.nextInt(colorSpaces.length)];
            fractions[i] = rand.nextFloat();
        }
        Arrays.sort(fractions);

        g2.setColor(Color.GRAY);// 设置边框色
        g2.fillRect(0, 0, w, h);

        Color c = getRandColor(200, 250);
        g2.setColor(c);// 设置背景色
        g2.fillRect(0, 2, w, h-4);

        //绘制干扰线
        Random random = new Random();
        g2.setColor(getRandColor(160, 200));// 设置线条的颜色
        for (int i = 0; i < 20; i++) {
            int x = random.nextInt(w - 1);
            int y = random.nextInt(h - 1);
            int xl = random.nextInt(6) + 1;
            int yl = random.nextInt(12) + 1;
            g2.drawLine(x, y, x + xl + 40, y + yl + 20);
        }

        // 添加噪点
        float yawpRate = 0.05f;// 噪声率
        int area = (int) (yawpRate * w * h);
        for (int i = 0; i < area; i++) {
            int x = random.nextInt(w);
            int y = random.nextInt(h);
            int rgb = getRandomIntColor();
            image.setRGB(x, y, rgb);
        }

        shear(g2, w, h, c);// 使图片扭曲

        g2.setColor(getRandColor(100, 160));
        int fontSize = h-4;
        Font font = new Font("Algerian", Font.ITALIC, fontSize);
        g2.setFont(font);
        char[] chars = code.toCharArray();
        for(int i = 0; i < verifySize; i++){
            AffineTransform affine = new AffineTransform();
            affine.setToRotation(Math.PI / 4 * rand.nextDouble() * (rand.nextBoolean() ? 1 : -1), (w / verifySize) * i + fontSize/2, h/2);
            g2.setTransform(affine);
            g2.drawChars(chars, i, 1, ((w-10) / verifySize) * i + 5, h/2 + fontSize/2 - 10);
        }

        g2.dispose();
        ImageIO.write(image, "jpg", os);
    }

    private static Color getRandColor(int fc, int bc) {
        if (fc > 255)
            fc = 255;
        if (bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }

    private static int getRandomIntColor() {
        int[] rgb = getRandomRgb();
        int color = 0;
        for (int c : rgb) {
            color = color << 8;
            color = color | c;
        }
        return color;
    }

    private static int[] getRandomRgb() {
        int[] rgb = new int[3];
        for (int i = 0; i < 3; i++) {
            rgb[i] = random.nextInt(255);
        }
        return rgb;
    }

    private static void shear(Graphics g, int w1, int h1, Color color) {
        shearX(g, w1, h1, color);
        shearY(g, w1, h1, color);
    }

    private static void shearX(Graphics g, int w1, int h1, Color color) {

        int period = random.nextInt(2);

        boolean borderGap = true;
        int frames = 1;
        int phase = random.nextInt(2);

        for (int i = 0; i < h1; i++) {
            double d = (double) (period >> 1)
                    * Math.sin((double) i / (double) period
                    + (6.2831853071795862D * (double) phase)
                    / (double) frames);
            g.copyArea(0, i, w1, 1, (int) d, 0);
            if (borderGap) {
                g.setColor(color);
                g.drawLine((int) d, i, 0, i);
                g.drawLine((int) d + w1, i, w1, i);
            }
        }

    }

    private static void shearY(Graphics g, int w1, int h1, Color color) {

        int period = random.nextInt(40) + 10; // 50;

        boolean borderGap = true;
        int frames = 20;
        int phase = 7;
        for (int i = 0; i < w1; i++) {
            double d = (double) (period >> 1)
                    * Math.sin((double) i / (double) period
                    + (6.2831853071795862D * (double) phase)
                    / (double) frames);
            g.copyArea(i, 0, 1, h1, 0, (int) d);
            if (borderGap) {
                g.setColor(color);
                g.drawLine(i, (int) d, i, 0);
                g.drawLine(i, (int) d + h1, i, h1);
            }

        }

    }
//    public static void main(String[] args) throws IOException{
//        File dir = new File("e:/verifies");// 生成的验证码存放目录
//        int w = 200, h = 80;
//
//        String verifyCode = generateVerifyCode(4);
//        File file = new File(dir, verifyCode + ".jpg");
//        outputImage(w, h, file, verifyCode);
//
//        /*String verifyCode = generateVerifyCode(4);
//        System.out.println(verifyCode);*/
//
//    }
}
