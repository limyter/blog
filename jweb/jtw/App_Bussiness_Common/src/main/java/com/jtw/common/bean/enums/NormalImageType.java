package com.jtw.common.bean.enums;

import org.springframework.http.MediaType;

/**
 * DESCRIPT:系统允许的图片上传类型
 * image/gif, image/png, image/jpeg, image/bmp,  image/x-icon, image/vnd.microsoft.icon
 *
 * @author cjsky666
 * @date 2018/11/19 15:50
 */
public enum  NormalImageType {

    IMAGE_GIF(MediaType.IMAGE_GIF_VALUE),
    IMAGE_PNG(MediaType.IMAGE_PNG_VALUE),
    IMAGE_JPEG(MediaType.IMAGE_JPEG_VALUE),
    IMAGE_ICON("image/x-icon"),
    IMAGE_BMP("image/bmp"),
    IMAGE_MICRO_ICON("mage/vnd.microsoft.icon")
    ;
    private String value;
    NormalImageType(String value) {
        this.value = value;
    }
}
