package com.jtw.common.bean.vo;

import lombok.Data;

import java.util.List;

/**
 * DESCRIPT: 翻页实体的扩展类，
 * 主要是为了统一管理翻页查询中的筛选条件
 * @author cjsky666
 * @date 2018/10/31 16:36
 */
@Data
public class FindPagingVo extends PagingVo{
    private List<FindParamVo> findParams;
}
