package com.jtw.sys.vo.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:系统用户角色修改
 *
 * @author cjsky666
 * @date 2018/9/25 16:28
 */
@Data
public class SysUserRolesModifyReq implements Serializable {
    private static final long serialVersionUID = -6446928919364186639L;
    @NotNull(message = "用户id不能为空")
    private Long userId;
    private Integer[] roleList = new Integer[]{};
}
