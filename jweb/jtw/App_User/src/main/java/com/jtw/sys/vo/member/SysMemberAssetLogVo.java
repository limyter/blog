package com.jtw.sys.vo.member;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT:会员资产变更记录系统业务类
 *
 * @author cjsky666
 * @date 2019/1/24 15:35
 */
@Data
public class SysMemberAssetLogVo extends MemberAssetLogVo implements Serializable {
    private static final long serialVersionUID = -2034945415078701959L;
    private String userName;
}
