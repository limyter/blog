package com.jtw.sys.mapper.perm;

import com.MyMapper;
import com.jtw.sys.model.perm.TSysPermGroup;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/15 14:19
 */
public interface SysPermGroupMapper extends MyMapper<TSysPermGroup> {
}
