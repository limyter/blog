package com.jtw.sys.vo.information;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:46
 */
@Data
public class SysInformationAddReq implements Serializable {
    private static final long serialVersionUID = -1272484132575742081L;
    private String author;
    private String title;
    private Integer state = 0;
    @NotBlank(message = "资讯内容不能为空")
    private String content;
    private String descript;
}
