package com.jtw.sys.mapper.member;

import com.MyMapper;
import com.jtw.sys.model.member.TSysMemberAssetLog;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/7/21 12:15
 */
public interface MemberAssetLogMapper extends MyMapper<TSysMemberAssetLog> {
    @Select("SELECT COUNT(0) FROM `t_sys_member_asset_log` WHERE `USER_ID` = #{userId} AND `TYPE`= 7 AND TO_DAYS(CREATE_TIME) = TO_DAYS(#{currentDate})")
    int checkTodayWxgzhShare(@Param("userId")Long userId, @Param("currentDate")Date currentDate);
}
