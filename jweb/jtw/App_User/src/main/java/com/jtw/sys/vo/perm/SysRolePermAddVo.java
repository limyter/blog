package com.jtw.sys.vo.perm;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/18 18:40
 */
@Data
public class SysRolePermAddVo implements Serializable {
    private static final long serialVersionUID = -6301755352089032242L;
    private Integer id;
    private String desc;
}
