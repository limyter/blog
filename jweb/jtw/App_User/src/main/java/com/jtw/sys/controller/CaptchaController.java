package com.jtw.sys.controller;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.util.ImageCaptchaUtil;
import com.jtw.sys.constants.Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api(tags = "验证码模块")
@Controller
@RequestMapping("/sys/captcha")
public class CaptchaController {
    @ApiOperation(notes = "获取系统登录验证码",value = "登录验证码")
    @GetMapping(value = "/getLoginCaptcha",produces = MediaType.IMAGE_JPEG_VALUE)
    @PermAnno(sort = 4,group = 1,open = true)
    public void getLoginCaptcha(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        String sRand = ImageCaptchaUtil.build(response);
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");
        String sRand = ImageCaptchaUtil.outputVerifyImage(100,40,response.getOutputStream(),4);
        request.getSession().setAttribute(Constant.KEY_CAPTCHA, sRand);
    }
}
