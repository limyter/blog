package com.jtw.sys.util;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jtw.sys.model.task.TSysTask;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:定时任务启动模型
 *
 * @author cjsky666
 * @date 2018/11/23 15:54
 */
@Data
public class TaskModel extends TSysTask implements Serializable {
    public static final boolean STATUS_RUNNING = true;  //正在运行
    public static final boolean STATUS_NOT_RUNNING = false; // 已停止
    public static final boolean CONCURRENT_IS = true;
    public static final boolean CONCURRENT_NOT = false;

    private Integer id;

    private String springId;
}
