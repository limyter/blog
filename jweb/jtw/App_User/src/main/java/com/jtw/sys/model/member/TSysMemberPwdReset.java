package com.jtw.sys.model.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT: 用户密码重置申请实体
 *
 * @author cjsky666
 * @date 2019/2/18 17:41
 */
@Data
@Table(name = "`t_sys_member_pwd_reset`")
public class TSysMemberPwdReset implements Serializable {
    private static final long serialVersionUID = 4613324213691837279L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Long id;
    private String userName;
    private String password;
    private String reason;
    private Integer state;
    private String descript;
    private String operator;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
