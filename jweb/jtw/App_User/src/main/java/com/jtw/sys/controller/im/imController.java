package com.jtw.sys.controller.im;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.constant.IM_Room;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.service.user.SysUserServiceImpl;
import com.jtw.sys.vo.user.SysUserInfoBaseVo;
import com.jtw.ws.WsHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/19 9:15
 */
@Api(tags = "在线客服管理模块")
@RestController
@Slf4j
@Validated //开启访问参数校验
@RequestMapping(value = "/sys/im")
public class imController {


    @Autowired
    SysUserServiceImpl sysUserServiceImpl;

    @ApiOperation(notes = "客服管理组",value = "新建客服房间")
    @PermAnno(sort = 1, group = 14)
    @PostMapping(value = "/addService")
    public Message addService(HttpServletRequest request, String name) throws IOException, FileUploadException, TikaException, SAXException {
//        System.out.println("=====================");
//        System.out.println(WsHandler.roomMap);
//        System.out.println("=====================");

        SysUserInfoBaseVo sysUserInfoBaseVo = (SysUserInfoBaseVo) SecurityUtils.getSubject().getSession().getAttribute(Constant.KEY_USER_INFO);
        if(WsHandler.roomMap.get("s"+sysUserInfoBaseVo.getId())==null){
            IM_Room im_room =   new IM_Room();
            im_room.setGroupId("s"+sysUserInfoBaseVo.getId());
            im_room.setCreateTime(new Date());
            im_room.setUserId("s"+sysUserInfoBaseVo.getId());
            im_room.setName("客服大厅");
            im_room.setNickName(StringUtils.isBlank(name)?sysUserInfoBaseVo.getId()+"号客服":name);
            WsHandler.roomMap.put(im_room.getGroupId(),im_room);
        }
        return ReqCode.AddSuccess.getMessage();
    }
}
