package com.jtw.sys.vo.system;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/20 22:02
 */
@Data
public class SysConfigCustomVo implements Serializable {
    private String keyName;
    private String keyValue;
}
