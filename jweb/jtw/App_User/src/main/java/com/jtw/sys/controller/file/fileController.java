package com.jtw.sys.controller.file;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.FilePathEnums;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.util.FileUtils;
import com.jtw.common.util.UploadUtil;
import com.jtw.sys.service.user.SysUserServiceImpl;
import com.jtw.sys.vo.system.SysLogFilePathReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/19 9:15
 */
@Api(tags = "文件上传管理模块")
@RestController
@Slf4j
@Validated //开启访问参数校验
@RequestMapping(value = "/sys/file")
public class fileController {

    @Value("${logfilePath}")
    private String logfilePath;
    @Value("${logfileName}")
    private String logfileName;

    private File logFile = null;

    @Autowired
    SysUserServiceImpl sysUserServiceImpl;

    @Value("${fileSaveDirectory}")
    private  String fileSaveDirectory;

    @Value("${fileSavePath}")
    private  String fileSavePath;

    @ApiOperation(notes = "系统登陆可用组",value = "头像上传接口")
    @PermAnno(sort = 4, group = 99)
    @PostMapping(value = "/uploadSingleHeadImage")
    public Message uploadSingleHeadImage(HttpServletRequest request) throws IOException, FileUploadException, TikaException, SAXException {

        List<Map<String,String>> resultList= new ArrayList<Map<String,String>>();

        resultList = UploadUtil.
                UploadImageAndCreateThumbNailImage(resultList, request,
                fileSaveDirectory+FilePathEnums.headImageUploadPath.getPath(),
                fileSavePath+FilePathEnums.headImageUploadPath.getPath(),null,null);
        if(resultList.size()<1){
            throw ReqCode.UploadError.getException();
        }
        return ReqCode.UploadSuccess.getMessage(resultList.get(0));
    }

    @ApiOperation(notes = "单文件上传",value = "银行卡图片上传接口")
    @PostMapping(value = "/uploadSysBankImage")
    @PermAnno(sort = 1, group = 7)
    public Message uploadSysBankImage(HttpServletRequest request) throws IOException, FileUploadException, TikaException, SAXException {

        List<Map<String,String>> resultList= new ArrayList<Map<String,String>>();

        resultList = UploadUtil.
                UploadImageAndCreateThumbNailImage(resultList, request,
                        fileSaveDirectory+FilePathEnums.PayImageUploadPath.getPath(),
                        fileSavePath+FilePathEnums.PayImageUploadPath.getPath(),200,240);
        if(resultList.size()<1){
            throw ReqCode.UploadError.getException();
        }
        return ReqCode.UploadSuccess.getMessage(resultList.get(0));
    }

    @ApiOperation(notes = "单文件上传",value = "封面上传接口")
    @PostMapping(value = "/uploadGameImage")
    @PermAnno(sort = 1, group = 7)
    public Message uploadGameImage(HttpServletRequest request) throws IOException, FileUploadException, TikaException, SAXException {

        List<Map<String,String>> resultList= new ArrayList<Map<String,String>>();

        resultList = UploadUtil.
                UploadImageAndCreateThumbNailImage(resultList, request,
                        fileSaveDirectory+FilePathEnums.OtherImageUploadPath.getPath(),
                        fileSavePath+FilePathEnums.OtherImageUploadPath.getPath(),720,560);
        if(resultList.size()<1){
            throw ReqCode.UploadError.getException();
        }
        return ReqCode.UploadSuccess.getMessage(resultList.get(0));
    }

    @ApiOperation(notes = "单文件上传",value = "客户端背景图管理")
    @PermAnno(sort = 4, group = 7)
    @PostMapping(value = "/uploadClientBgImage")
    public Message uploadClientBgImage(HttpServletRequest request) throws IOException, FileUploadException, TikaException, SAXException {
        //1为移动端背景图，2为手机端背景图
        List<Map<String,String>> resultList= new ArrayList<Map<String,String>>();
        resultList = UploadUtil.
                UploadImageAndCreateThumbNailImage(resultList, request,
                        fileSaveDirectory+FilePathEnums.OtherImageUploadPath.getPath(),
                        fileSavePath+FilePathEnums.OtherImageUploadPath.getPath(),null,null);
        if(resultList.size()<1){
            throw ReqCode.UploadError.getException();
        }
        return ReqCode.UploadSuccess.getMessage(resultList.get(0));
    }

    @ApiOperation(notes = "单文件上传",value = "网站logo修改")
    @PermAnno(sort = 4, group = 7)
    @PostMapping(value = "/uploadWebLogoImage")
    public Message uploadWebLogoImage(HttpServletRequest request) throws IOException, FileUploadException, TikaException, SAXException {
        List<Map<String,String>> resultList= new ArrayList<Map<String,String>>();
        resultList = UploadUtil.
                UploadImageAndCreateThumbNailImage(resultList, request,
                        fileSaveDirectory+FilePathEnums.OtherImageUploadPath.getPath(),
                        fileSavePath+FilePathEnums.OtherImageUploadPath.getPath(),null,null);
        if(resultList.size()<1){
            throw ReqCode.UploadError.getException();
        }
        return ReqCode.UploadSuccess.getMessage(resultList.get(0));
    }

    @ApiOperation(notes = "日志文件管理",value = "获取所有日志文件")
    @PermAnno(sort = 4, group = 7)
    @GetMapping(value = "/getAllSysLogFile")
    public Message getAllSysLogFile(HttpServletRequest request) throws IOException, FileUploadException, TikaException, SAXException {
        return ReqCode.SelectSuccess.getMessage(Arrays.asList(FileUtils.getFileTree(new File(logfilePath))));
    }

    @ApiOperation(notes = "日志文件管理",value = "删除日志文件")
    @PermAnno(sort = 7, group = 7)
    @PostMapping(value = "/delAllSysLogFile")
    public Message delAllSysLogFile(HttpServletRequest request,@Valid SysLogFilePathReq sysLogFilePathVo) throws IOException, FileUploadException, TikaException, SAXException {
        if(logFile==null){
            logFile  = new File(logfilePath);
        }
        for(String path:sysLogFilePathVo.getPaths()){
            File tFile = new File(path);
            if(tFile.getAbsolutePath().equals(logFile.getAbsolutePath())){
                continue;
            }
            if(!tFile.getAbsolutePath().startsWith(logFile.getAbsolutePath())){
                continue;
            }
            if(tFile.getAbsolutePath().endsWith(logfileName)){
                continue;
            }
            if(tFile.exists()){
                tFile.delete();
//                System.out.println(tFile.getAbsolutePath());
//                System.out.println("删除成功"+tFile.delete());
            }
        }
        return ReqCode.SubmitSuccess.getMessage();
    }
}
