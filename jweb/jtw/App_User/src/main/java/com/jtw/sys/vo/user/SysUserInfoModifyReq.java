package com.jtw.sys.vo.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT: 修改系统用户账号基本信息
 * @author cjsky666
 * @date 2018/9/25 16:34
 */
@Data
public class SysUserInfoModifyReq implements Serializable {
    private static final long serialVersionUID = -2514536455639974358L;
    @NotNull(message = "id不能改为空")
    private Long id;
    private String nickName;
    private String headImage;
    private Integer sex;
    private String thumbNailImage;
    private Integer age;
    private String mobile;
    private String email;
    private String qq;
    private String address;
    private String desc;
}
