package com.jtw.sys.model.menu;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT: 管理后台系统菜单实例
 *
 * @author cjsky666
 * @date 2018/10/10 10:37
 */
@Data
@Table(name = "`t_sys_menu`")
public class TSysMenu implements Serializable {
    private static final long serialVersionUID = -8590642087093494133L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private String name;
    private Integer parent;
    private String desc;
    private Integer group;
    private Integer type;
    private Integer sort;
    private String path;
    private Boolean open;
    private String operator;
    private String operatorIp;
    private Date createTime;
    private Date updateTime;
}
