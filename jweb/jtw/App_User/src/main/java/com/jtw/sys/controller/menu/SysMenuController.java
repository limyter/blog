package com.jtw.sys.controller.menu;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.model.menu.TSysMenu;
import com.jtw.sys.service.menu.SysMenuServiceImpl;
import com.jtw.sys.vo.menu.SysMenuAddReq;
import com.jtw.sys.vo.menu.SysMenuAddVo;
import com.jtw.sys.vo.menu.SysMenuRoleMarkModifyReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/11 9:00
 */
@Api(tags = "系统菜单管理模块")
@RestController
@Slf4j
@Validated //开启访问参数校验
@RequestMapping(value = "/sys/menu")
public class SysMenuController {
    @Autowired
    private SysMenuServiceImpl sysMenuService;

    @ApiOperation(notes = "系统菜单管理", value = "获取所有系统菜单")
    @GetMapping(value = "/getAllSysMenus")
    @PermAnno(sort = 1, group = 2)
    public Message getAllSysMenus() {
        return ReqCode.SelectSuccess.getMessage(sysMenuService.getAll());
    }

    @ApiOperation(notes = "系统菜单管理", value = "查询系统菜单表中的记录")
    @GetMapping(value = "/findAllSysMenus")
    @PermAnno(sort = 2, group = 2)
    public Message findAllSysMenus(PagingVo pagingVo) {
        return ReqCode.SelectSuccess.getMessage(sysMenuService.findAll(pagingVo));
    }

    @ApiOperation(notes = "系统菜单管理", value = "添加系统菜单")
    @PostMapping(value = "/addSysMenu")
    @PermAnno(sort = 3, group = 2)
    public Message addSysMenu(@Valid SysMenuAddReq sysMenuAddReq) {
        TSysMenu tSysMenu = new TSysMenu();
        BeanUtils.copyProperties(sysMenuAddReq, tSysMenu);
        sysMenuService.save(tSysMenu);
        return ReqCode.AddSuccess.getMessage();
    }

    @ApiOperation(notes = "系统菜单管理", value = "修改单条菜单信息")
    @PostMapping(value = "/modifySysMenu")
    @PermAnno(sort = 4, group = 2)
    public Message modifySysMenu(@Valid SysMenuAddVo sysMenuAddVo) {
        TSysMenu tSysMenu = new TSysMenu();
        BeanUtils.copyProperties(sysMenuAddVo, tSysMenu);
        sysMenuService.update(tSysMenu);
        return ReqCode.UpdateSuccess.getMessage();
    }

    @ApiOperation(notes = "系统菜单管理", value = "删除单条系统菜单")
    @PostMapping(value = "/delSysMenu")
    @PermAnno(sort = 5, group = 2)
    public Message delSysMenu(@NotNull(message = "菜单id不能为空") Integer menuId) {
        sysMenuService.delete(menuId);
        return ReqCode.DelSuccess.getMessage();
    }

    @ApiOperation(notes = "系统菜单管理", value = "获取角色所有菜单并标记")
    @GetMapping(value = "/getAllSysMenuTreeMarkByRoleId")
    @PermAnno(sort = 6, group = 2)
    public Message getAllSysMenuTreeMarkByRoleId(@NotNull(message = "角色id不能为空") Integer roleId) {
        return ReqCode.SelectSuccess.getMessage(sysMenuService.getAllSysMenuTreeMarkByRoleId(roleId));
    }

    @ApiOperation(notes = "系统菜单管理", value = "修改单个角色菜单")
    @PostMapping(value = "/modifyRoleMenus")
    @PermAnno(sort = 6, group = 2)
    public Message modifyRoleMenus(@Valid SysMenuRoleMarkModifyReq sysMenuRoleMarkModifyReq) {
        sysMenuService.modifyRoleMenus(sysMenuRoleMarkModifyReq);
        return ReqCode.UpdateSuccess.getMessage();
    }

    @ApiOperation(notes = "系统登陆可用组", value = "获取用户的菜单树")
    @GetMapping(value = "/getAllMenusTreeByAccount")
    @PermAnno(sort = 2, group = 99)
    public Message getAllMenusTreeByAccount() {
        Long userId = (Long) SecurityUtils.getSubject().getSession().getAttribute(Constant.USER_PRIMARY_KEY);
        return ReqCode.SelectSuccess.getMessage(sysMenuService.getAllMenusTreeByUserId(userId));
    }
}
