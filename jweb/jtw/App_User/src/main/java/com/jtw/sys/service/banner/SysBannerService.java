package com.jtw.sys.service.banner;

import com.jtw.common.baseService.BaseService;
import com.jtw.sys.model.banner.TSysBanner;
import com.jtw.sys.vo.banner.SysBannerVo;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 13:57
 */
public interface SysBannerService extends BaseService<TSysBanner> {
    List<SysBannerVo> getAllMobileClientBanner();
}
