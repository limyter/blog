package com.jtw.sys.vo.asset;

import com.jtw.sys.model.member.TSysMemberAsset;
import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT: 系统会员资产信息业务类
 *
 * @author cjsky666
 * @date 2019/2/4 0:13
 */
@Data
public class SysMemberAssetVo extends TSysMemberAsset implements Serializable {
    private static final long serialVersionUID = -4653396673877310734L;
    private String userName;
}
