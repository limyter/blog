package com.jtw.sys.mapper.member;

import com.MyMapper;
import com.jtw.sys.model.member.TSysMemberCheckInReward;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/7/22 9:21
 */
public interface MemberCheckInRewardMapper extends MyMapper<TSysMemberCheckInReward> {
}
