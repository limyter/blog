package com.jtw.sys.mapper.log;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.log.TSysAccessLog;
import org.apache.ibatis.annotations.*;

import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/21 13:06
 */
public interface SysAccessMapper extends MyMapper<TSysAccessLog> {

    @Insert("INSERT INTO `t_sys_access_log`" +
            " (USER_ID,USER_NAME,URL_NAME, IP, PORT, URL, ADDRESS, MAC, PARAMS, HTTP_METHOD, BROWSER_TYPE, OS, RENDER_ENGINE, BROWSER_VERSION, BROWSER_NAME)" +
            " VALUES" +
            " (#{userId},#{userName},#{urlName}, #{ip}, #{port}, #{url}, #{address}, #{mac}, #{params}, #{httpMethod}, #{browserType}, #{os}, #{renderEngine}, #{browserVersion}, #{browserName})")
    @Options(useGeneratedKeys = true,keyColumn = "ID")
    int saveBackId(TSysAccessLog tSysAccessLog);


    @Select("SELECT * FROM `t_sys_access_log` ${whereSQL}")
    Page<TSysAccessLog> finAll(@Param("whereSQL") String whereSQL);

    @Delete("DELETE FROM `t_sys_access_log` WHERE CREATE_TIME <= #{end} ")
    Integer delBeforeDay(@Param("end") Date end);
}
