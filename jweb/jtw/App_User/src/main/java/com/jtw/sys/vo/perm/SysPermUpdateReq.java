package com.jtw.sys.vo.perm;

import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:更新单个权限信息的业务模型
 *
 * @author cjsky666
 * @date 2018/9/20 15:26
 */
@Data
public class SysPermUpdateReq implements Serializable {
    private static final long serialVersionUID = -3378228430292658539L;
    @NotNull(message = "权限id不能为空")
    private Integer id;

    @NotBlank(message = "权限名称不能为空")
    private String name;

    @NotNull(message = "权限类型")
    private Integer type;

    @NotBlank(message = "权限映射路径")
    private String url;

    @NotBlank(message = "权限备注不能为空")
    private String desc;

    @NotBlank(message = "请求类型不能为空")
    private String httpMethod;

    @NotBlank(message = "类方法名不能为空")
    private String methodName;

    @NotBlank(message = "类包名不能为空")
    private String className;

    private Integer group;
    @NotNull(message = "不能为空")
    @DecimalMin(value = "0",message = "排序数值不能小于0的整数")
    private Integer sort;

    @NotNull(message = "权限状态必须设置")
    @DecimalMin(value = "0",message = "启用状态只能是0或者1")
    @DecimalMax(value = "1",message = "启用状态只能是0或者1")
    private Integer state;

    @NotNull(message = "是否公共接口必须设置")
    private Boolean open;
}
