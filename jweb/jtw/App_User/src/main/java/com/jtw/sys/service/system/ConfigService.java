package com.jtw.sys.service.system;

import com.jtw.common.baseService.BaseService;
import com.jtw.sys.model.system.TSysConfig;

import java.util.Map;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/18 16:26
 */
public interface ConfigService extends BaseService<TSysConfig> {
    Map<String,String> getAllConfig();

    TSysConfig getByKeyName(String keyName);
}
