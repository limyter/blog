package com.jtw.sys.model.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jtw.conf.shiro.model.BaseRole;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:shiro角色表
 * id与用户表同相同
 *
 * @author cjsky666
 * @date 2018/9/14 14:48
 */


@Data
@Table(name = "`t_sys_role`")
public class TSysRole implements Serializable {
    private static final long serialVersionUID = 917183036448382170L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private String name;
    private String desc;
    private String operator;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;


}