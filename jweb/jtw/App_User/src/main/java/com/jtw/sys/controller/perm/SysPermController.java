package com.jtw.sys.controller.perm;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.sys.model.perm.TSysPerm;
import com.jtw.sys.service.perm.SysPermServiceImpl;
import com.jtw.sys.vo.perm.SysPermUpdateReq;
import com.jtw.sys.vo.perm.SysPermVo;
import com.jtw.sys.vo.perm.SysRolePermAddReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * DESCRIPT:系统权限操作类
 *
 * @author cjsky666
 * @date 2018/9/12 13:41
 */
@Api(tags = "系统权限管理模块")
@Slf4j
@Validated
@RestController
@RequestMapping(value = "/sys/perm")
public class SysPermController {

    @Autowired
    WebApplicationContext applicationContext;

    @Autowired
    SysPermServiceImpl sysPermService;

    @ApiOperation(notes = "系统权限管理",value = "添加单个系统权限")
    @PostMapping(value = "/addSysPerm")
    @PermAnno(sort = 1,group=3)
    public Message addSysPerm(@Valid SysPermVo sysPermVo){
        TSysPerm sysPerm = new TSysPerm();
        BeanUtils.copyProperties(sysPermVo,sysPerm);
        sysPermService.save(sysPerm);
        return ReqCode.AddSuccess.getMessage();
    }

//    @ApiOperation(value = "获取系统权限映射",value = "获取所有系统com.jtw下访问映射信息接口，只能赋予运维和超级管理员")
//    @GetMapping(value = "/getAllMapping")
//    public Message getAllMapping() {
//        //获取当前数据库内所有已登记的接口
//        List<String> permList = sysPermService.getAllPermUrl();
//        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
//        // 获取url与类和方法的对应信息
//        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
//        List<SysPermVo> list = new ArrayList<SysPermVo>();
////      循环当前系统映射接口
//        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : map.entrySet()) {
//            RequestMappingInfo information = m.getKey();
//            HandlerMethod method = m.getValue();
//            PatternsRequestCondition p = information.getPatternsCondition();
//            String className = method.getMethod().getDeclaringClass().getName();
//
//            if (!className.startsWith("com.jtw.controller")) {
//                continue;
//            }
//
//            SysPermVo sysPermVo = new SysPermVo();
//            sysPermVo.setClassName(className);
//            sysPermVo.setMethodName(method.getMethod().getName());
//            sysPermVo.setName(method.getMethod().getName());
//
//            for (String url : p.getPatterns()) {
//                if(!permList.contains(url)){
//                    sysPermVo.setUrl(url);
//                    RequestMethodsRequestCondition methodsCondition = information.getMethodsCondition();
//                    for (RequestMethod requestMethod : methodsCondition.getMethods()) {
//                        sysPermVo.setHttpMethod(requestMethod.toString());
//                    }
//                    list.add(sysPermVo);
//                }
//            }
//        }
//        return ReqCode.SelectSuccess.getMessage(list);
//    }

    @ApiOperation(notes = "系统权限管理",value = "修改单个角色权限")
    @PostMapping(value = "/modifyRolePerms")
    @PermAnno(sort = 2,group=3)
    public Message modifyRolePerms(@Valid SysRolePermAddReq rolePermReq){
        sysPermService.modifyRolePerms(rolePermReq.getRoleId(),rolePermReq.getPerms());
        return ReqCode.UpdateSuccess.getMessage();
    }

    @ApiOperation(notes = "系统权限管理",value = "获取单个角色未拥有的权限")
    @GetMapping(value = "/getAllRolePermsByRoleIdNotHave")
    @PermAnno(sort = 3,group=3)
    public Message getAllRolePermsByRoleIdNotHave(@NotNull(message = "角色id不能为空" ) Integer roleId) {
        return ReqCode.SelectSuccess.getMessage(sysPermService.getAllRolePermsByRoleIdNotHave(roleId));
    }

    @ApiOperation(notes = "系统权限管理",value = "获取单个角色的权限树并标记")
    @GetMapping(value = "/getAllSysPermTreeMarkByRoleId")
    @PermAnno(sort = 4,group=3)
    public Message getAllSysPermTreeMarkByRoleId(@NotNull(message = "角色id不能为空" ) Integer roleId) {
        return ReqCode.SelectSuccess.getMessage(sysPermService.getAllSysPermTreeMarkByRoleId(roleId));
    }

    @ApiOperation(notes = "系统权限管理",value = "获取单个角色所有权限")
    @GetMapping(value = "/getAllRolePermsByRoleId")
    @PermAnno(sort = 5,group=3)
    public Message getAllRolePermsByRoleId(@NotNull(message = "角色id不能为空" ) Integer roleId) {
        return ReqCode.SelectSuccess.getMessage(sysPermService.getAllRolePermsByRoleId(roleId));
    }

    @ApiOperation(notes = "系统权限管理",value = "查询所有系统权限")
    @GetMapping(value = "/findAllSysPerms")
    @PermAnno(sort = 6,group=3)
    public Message findAllSysPerms(PagingVo pagingVo) {
        return ReqCode.SelectSuccess.getMessage(sysPermService.findAll(pagingVo));
    }

    @ApiOperation(notes = "系统权限管理",value = "修改单条权限基本信息")
    @PostMapping(value = "/modifySysPerm")
    @PermAnno(sort = 7,group=3)
    public Message modifySysPerm(@Valid SysPermUpdateReq sysPermUpdateReq) {
        TSysPerm sysPerm = new TSysPerm();
        BeanUtils.copyProperties(sysPermUpdateReq,sysPerm);
        sysPermService.updateSysPerm(sysPerm);
        return ReqCode.UpdateSuccess.getMessage();
    }
}
