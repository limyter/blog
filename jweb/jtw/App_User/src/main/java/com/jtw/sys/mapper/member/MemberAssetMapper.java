package com.jtw.sys.mapper.member;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.member.TSysMemberAsset;
import com.jtw.sys.vo.asset.SysMemberAssetVo;
import com.jtw.sys.vo.member.MemberAssetLogVo;
import com.jtw.sys.vo.member.MemberAssetRankVo;
import com.jtw.sys.vo.member.MemberAssetVo;
import com.jtw.sys.vo.member.SysMemberAssetLogVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/7/21 12:14
 */
public interface MemberAssetMapper extends MyMapper<TSysMemberAsset> {
    @Update("UPDATE `t_sys_member_asset` SET `BALANCE` = #{asset.balance},`FREEZE_BALANCE` = #{asset.freezeBalance} WHERE `ID` = #{asset.id} AND `UPDATE_TIME` = #{asset.updateTime}")
    int updateByVersion(@Param("asset") TSysMemberAsset asset);
    @Select("SELECT `ID`,`BALANCE`,`FREEZE_BALANCE`,`UPDATE_TIME` FROM `t_sys_member_asset` where `ID` = #{userId}")
    MemberAssetVo getOneByUserId(@Param("userId") Long userId);
    @Select("SELECT * FROM `t_sys_member_asset_log` ${whereSQL}")
    Page<MemberAssetLogVo> findAllMyAssetLog(@Param("whereSQL") String whereSQL);
    @Select("SELECT `t_sys_member_asset_log`.`*`,`t_sys_user`.`USER_NAME` FROM `t_sys_member_asset_log` left join `t_sys_user` on `t_sys_member_asset_log`.`USER_ID` = `t_sys_user`.`ID` ${whereSQL}")
    Page<SysMemberAssetLogVo> findAllMemberAssetChangeLog(@Param("whereSQL") String whereSQL);
    @Select("SELECT `t_sys_member_asset`.`*`,`t_sys_user`.`USER_NAME` FROM `t_sys_member_asset` left join `t_sys_user` on `t_sys_member_asset`.`ID` = `t_sys_user`.`ID` ${whereSQL}")
    Page<SysMemberAssetVo> findAllMemberAssetDetail(@Param("whereSQL") String whereSQL);

    @Select("SELECT\n" +
            "\t`t_sys_user_info`.`ID`,\n" +
            "\t`t_sys_user_info`.`NICK_NAME`,\n" +
            "\t`t_sys_user_info`.`HEAD_IMAGE`,\n" +
            "\t`t_sys_user_info`.`THUMB_NAIL_IMAGE`,\n" +
            "\t`temp`.`BALANCE`,\n" +
            "\t`temp`.`RANK_NO`,\n" +
            "\t`temp`.`UPDATE_TIME` \n" +
            "FROM\n" +
            "\t(\n" +
            "SELECT\n" +
            "\t`t_sys_member_asset`.*,\n" +
            "\t@RANK_NO := @RANK_NO + 1 AS `RANK_NO` \n" +
            "FROM\n" +
            "\t( SELECT @RANK_NO := 0 ) r,\n" +
            "\t`t_sys_member_asset` \n" +
            "WHERE\n" +
            "\t`t_sys_member_asset`.`ID` != 1 \n" +
            "ORDER BY\n" +
            "\t`t_sys_member_asset`.`BALANCE` DESC,\n" +
            "\t`t_sys_member_asset`.`UPDATE_TIME` ASC \n" +
            "\t) temp\n" +
            "\tLEFT JOIN `t_sys_user_info` ON `t_sys_user_info`.`ID` = `temp`.`ID` \n" +
            "ORDER BY\n" +
            "\tRANK_NO")
    Page<MemberAssetRankVo> findAllRank();
    @Select("SELECT\n" +
            "\t`t_sys_user_info`.`ID`,\n" +
            "\t`t_sys_user_info`.`NICK_NAME`,\n" +
            "\t`t_sys_user_info`.`HEAD_IMAGE`,\n" +
            "\t`t_sys_user_info`.`THUMB_NAIL_IMAGE`,\n" +
            "\t`temp`.`BALANCE`,\n" +
            "\t`temp`.`RANK_NO`,\n" +
            "\t`temp`.`UPDATE_TIME`\n" +
            "FROM\n" +
            "\t(\n" +
            "SELECT\n" +
            "\t`t_sys_member_asset`.*,\n" +
            "\t@RANK_NO := @RANK_NO + 1 AS `RANK_NO` \n" +
            "FROM\n" +
            "\t( SELECT @RANK_NO := 0 ) r,\n" +
            "\t`t_sys_member_asset`\n" +
            "WHERE\n" +
            "\t`t_sys_member_asset`.`ID` != 1 \n" +
            "ORDER BY\n" +
            "\t`t_sys_member_asset`.`BALANCE` DESC ,\n" +
            "\t`t_sys_member_asset`.`UPDATE_TIME` ASC\n" +
            "\t\n" +
            "\t) temp\n" +
            "LEFT JOIN `t_sys_user_info` ON `t_sys_user_info`.`ID` = `temp`.`ID`  WHERE `t_sys_user_info`.`ID`= #{userId} ORDER BY RANK_NO")
    MemberAssetRankVo getMyAssetRank(Integer userId);
}
