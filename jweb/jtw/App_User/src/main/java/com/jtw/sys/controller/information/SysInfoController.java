package com.jtw.sys.controller.information;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingItemAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.model.information.TSysInformation;
import com.jtw.sys.service.information.SysInformationServiceImpl;
import com.jtw.sys.vo.information.SysInformationAddReq;
import com.jtw.sys.vo.information.SysInformationUpdateReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/2 11:20
 */
@Api(tags = "资讯管理模块")
@RestController
@Slf4j
@Validated
@RequestMapping(value = "/sys/information")
public class SysInfoController {
    @Autowired
    private SysInformationServiceImpl sysInformationServiceImpl;

    @ApiOperation(notes = "资讯管理组", value = "添加资讯")
    @PostMapping("/addSysInformation")
    @PermAnno(sort = 1, group = 12)
    public Message addSysInformation(HttpServletRequest request,@Valid SysInformationAddReq sysInformationAddReq){
        TSysInformation tSysInformation = new TSysInformation();
        BeanUtils.copyProperties(sysInformationAddReq,tSysInformation);
        tSysInformation.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
        tSysInformation.setOperatorIp(request.getRemoteAddr());
        sysInformationServiceImpl.save(tSysInformation);
        return ReqCode.AddSuccess.getMessage();
    }

    @ApiOperation(notes = "资讯管理组", value = "修改资讯信息")
    @PostMapping("/modifySysInformation")
    @PermAnno(sort = 2, group = 12)
    public Message modifySysInformation(HttpServletRequest request, @Valid SysInformationUpdateReq sysInformationUpdateReq){
        TSysInformation tSysInformation = new TSysInformation();
        BeanUtils.copyProperties(sysInformationUpdateReq,tSysInformation);
        tSysInformation.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
        tSysInformation.setOperatorIp(request.getRemoteAddr());
        sysInformationServiceImpl.update(tSysInformation);
        return ReqCode.UpdateSuccess.getMessage();
    }

    @ApiOperation(notes = "资讯管理组", value = "查询所有资讯")
    @FindPagingAnno(
            items = {
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "operator"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "author"),
                    @FindPagingItemAnno(column = "state", fixedValue = {"0", "1","2"}, isString = false),
                    @FindPagingItemAnno(conditions = {">", "<", ">=", "<=", "="}, column = "createTime", prefix = "`t_sys_information`."),
            })
    @GetMapping("/findAllSysInformation")
    @PermAnno(sort = 3, group = 12)
    public Message findAll(@Valid FindPagingVo findPagingVo) {
        return ReqCode.Success.getMessage(sysInformationServiceImpl.findAll(findPagingVo));
    }


    @ApiOperation(notes = "资讯管理组", value = "获取单个资讯内容")
    @GetMapping("/getInformationById")
    @PermAnno(sort = 4, group = 12,open = true)
    public Message getInformationById(@NotNull(message = "id不能为空") Integer id) {
        return ReqCode.Success.getMessage(sysInformationServiceImpl.getInformationById(id));
    }

}
