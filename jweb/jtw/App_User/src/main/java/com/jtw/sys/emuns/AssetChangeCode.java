package com.jtw.sys.emuns;

/**
 * DESCRIPT: 系统账变枚举类
 *
 * @author cjsky666
 * @date 2019/1/21 17:57
 */
public enum AssetChangeCode {

    ADD_CREATE(0,"新建"),
    ADD_CHECK_IN(1,"签到奖励"),
    ADD_SHARE(7,"公众号分享奖励"),
    ADJUST_BALANCE(999,"积分调整"),
    ;
    private Integer type;
    private String descript;

    AssetChangeCode(Integer type, String descript) {
        this.type = type;
        this.descript = descript;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

}
