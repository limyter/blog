package com.jtw.sys.controller.task;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingItemAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.sys.model.task.TSysTask;
import com.jtw.sys.service.task.SysTaskServiceImpl;
import com.jtw.sys.util.TaskUtil;
import com.jtw.sys.vo.task.SysTaskAddReq;
import com.jtw.sys.vo.task.TaskModelVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.TextHorizontalOverflow;
import org.quartz.SchedulerException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * DESCRIPT: 定时任务控制器
 *
 * @author cjsky666
 * @date 2018/11/23 9:56
 */
@Api(tags = "定时任务管理模块")
@Slf4j
@Validated
@RestController
@RequestMapping(value = "/sys/task")
public class taskController {
    @Autowired
    private SysTaskServiceImpl sysTaskServiceImpl;

    @PermAnno(sort = 1, group = 6)
    @ApiOperation(notes = "定时任务管理",value = "新增定时任务")
    @PostMapping(value = "/addTask")
    public Message addTask(@Valid SysTaskAddReq sysTaskAddReq){
        TSysTask sysTask = new TSysTask();
        BeanUtils.copyProperties(sysTaskAddReq,sysTask);
        sysTaskServiceImpl.save(sysTask);
        return ReqCode.AddSuccess.getMessage();
    }

    @PermAnno(sort = 2, group = 6)
    @ApiOperation(notes = "定时任务管理",value = "暂停一个定时任务")
    @PostMapping(value = "/pauseTask")
    public Message pauseTask(@RequestParam @NotNull(message = "任务id不能为空") Long id){
        TSysTask sysTask = sysTaskServiceImpl.getById(id);
        if(sysTask == null){
            throw ReqCode.UnExistingDataException.getException();
        }
        try {
            TaskModelVo taskModelVo = new TaskModelVo();
            BeanUtils.copyProperties(sysTask, taskModelVo);
            TaskUtil.resumeTask(taskModelVo);
        } catch (SchedulerException e) {
            e.printStackTrace();
            throw ReqCode.TaskPasueException.getException();
        }
        return ReqCode.UpdateSuccess.getMessage();
    }

    @PermAnno(sort = 3, group = 6)
    @ApiOperation(notes = "定时任务管理",value = "恢复一个定时任务")
    @PostMapping(value = "/resumeTask")
    public Message resumeTask(@RequestParam @NotNull(message = "任务id不能为空") Long id){
        try {
            TSysTask sysTask = sysTaskServiceImpl.getById(id);
            if(sysTask == null){
                throw ReqCode.UnExistingDataException.getException();
            }
            TaskModelVo taskModelVo = new TaskModelVo();
            BeanUtils.copyProperties(sysTask, taskModelVo);
            TaskUtil.resumeTask(taskModelVo);
        } catch (SchedulerException e) {
            e.printStackTrace();
            throw ReqCode.TaskResumeException.getException();
        }
        return ReqCode.UpdateSuccess.getMessage();
    }

    @PermAnno(sort = 4, group = 6)
    @ApiOperation(notes = "定时任务管理",value = "查询所有计划任务")
    @GetMapping(value = "/findAllTask")
    @FindPagingAnno(
            items = {
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "name"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "groupName"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "cron"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "param"),
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "methodName"),
                    @FindPagingItemAnno(conditions = {"="}, column = "taskState",isString = false),
                    @FindPagingItemAnno(conditions = {"="}, column = "runState",isString = false)
            })
    public Message findAllTask(@Valid FindPagingVo findPagingVo){
        return ReqCode.SelectSuccess.getMessage(sysTaskServiceImpl.findAll(findPagingVo));
    }

    @PermAnno(sort = 5, group = 6)
    @ApiOperation(notes = "定时任务管理",value = "修改定时任务信息")
    @PostMapping(value = "/modifyTask")
    public Message modifyTask(@Valid TaskModelVo taskModelVo) throws SchedulerException {
        TSysTask sysTask = new TSysTask();
        BeanUtils.copyProperties(taskModelVo,sysTask);
        sysTaskServiceImpl.update(sysTask);
        return ReqCode.UpdateSuccess.getMessage();
    }

    @PermAnno(sort = 6, group = 6)
    @ApiOperation(notes = "定时任务管理",value = "开启或者关闭一个定时任务")
    @PostMapping(value = "/lockTask")
    public Message lockTask(@RequestParam @NotNull(message = "任务id不能为空") Long id) throws SchedulerException {
        TSysTask sysTask = sysTaskServiceImpl.getById(id);
        if(sysTask == null){
            throw ReqCode.UnExistingDataException.getException();
        }
        if(sysTask.getTaskState()){
            sysTask.setTaskState(false);
        }else{
            sysTask.setTaskState(true);
        }
        sysTaskServiceImpl.update(sysTask);
        return ReqCode.UpdateSuccess.getMessage();
    }
}
