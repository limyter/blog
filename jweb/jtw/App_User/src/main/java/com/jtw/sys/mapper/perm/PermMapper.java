package com.jtw.sys.mapper.perm;

import com.jtw.conf.shiro.model.BaseRole;
import com.jtw.conf.shiro.model.BaseUser;
import com.jtw.sys.model.perm.TSysPerm;
import org.apache.ibatis.annotations.Flush;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * DESCRIPT: 查询系统相关权限信息DAO
 *
 * @author cjsky666
 * @date 2018/9/3 15:18
 */
public interface PermMapper {

    @Select("SELECT * FROM t_sys_role" +
            " WHERE ID IN (" +
            " SELECT ROLE_ID" +
            " FROM `ct_sys_user_role`" +
            " WHERE USER_ID = (" +
            " SELECT ID FROM `t_sys_user`" +
            " WHERE USER_NAME = #{userName}))")
    Set<BaseRole> getRolesByUserName(String userName);

    @Select("SELECT * FROM `t_sys_user` WHERE USER_NAME = #{userName}")
    BaseUser getUserByUserName(String userName);

    @Select("SELECT * FROM `t_sys_perm` ORDER BY `GROUP`,`SORT`")
    List<TSysPerm> getAllPerm();

    @Select("SELECT * FROM `t_sys_role` WHERE ID IN (SELECT ROLE_ID FROM `ct_sys_role_perm` WHERE PERM_ID = #{id})")
    List<BaseRole> getRolesByPermId(Integer id);

    BaseUser getUserByMobile(@Param("id")String mobile);

    BaseUser getUserByWXAPP(@Param("id")String wxAppOpenId);

    @Select("SELECT `t_sys_user`.* FROM  `t_sys_user` LEFT JOIN `t_sys_user_open` ON `t_sys_user_open`.`USER_ID` = `t_sys_user`.`ID`  WHERE `t_sys_user_open`.`WX_GZH_OPEN_ID` = #{id}")
    BaseUser getUserByWXGZH(@Param("id") String wxGzhOpenId);

    BaseUser getUserByTXUNION(@Param("id")String txUnionId);

    BaseUser getUserByZFB(@Param("id")String zfbOpenId);

    BaseUser getUserByQQ(@Param("id")String qqOpenId);

    BaseUser getUserBySina(@Param("id")String sinaOpenId);
}
