package com.jtw.sys.controller.banner;

import com.jtw.common.anno.RequestAnno.PermAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingAnno;
import com.jtw.common.anno.findPagingAnno.FindPagingItemAnno;
import com.jtw.common.bean.Result.Message;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.model.banner.TSysBanner;
import com.jtw.sys.model.information.TSysInformation;
import com.jtw.sys.service.banner.SysBannerServiceImpl;
import com.jtw.sys.vo.banner.SysBannerAddReq;
import com.jtw.sys.vo.banner.SysBannerUpdateReq;
import com.jtw.sys.vo.information.SysInformationAddReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/2 11:20
 */
@Api(tags = "客户端设置模块")
@RestController
@Slf4j
@Validated
@RequestMapping(value = "/sys/banner")
public class SysBannerController {
    @Autowired
    private SysBannerServiceImpl sysBannerServiceImpl;

    @ApiOperation(notes = "客户端设置组", value = "添加轮播广告")
    @PostMapping("/addSysBanner")
    @PermAnno(sort = 1, group = 13)
    public Message addSysBanner(HttpServletRequest request,@Valid SysBannerAddReq sysBannerAddReq){
        TSysBanner tSysBanner = new TSysBanner();
        BeanUtils.copyProperties(sysBannerAddReq,tSysBanner);
        tSysBanner.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
        tSysBanner.setOperatorIp(request.getRemoteAddr());
        sysBannerServiceImpl.save(tSysBanner);
        return ReqCode.AddSuccess.getMessage();
    }

    @ApiOperation(notes = "客户端设置组", value ="修改轮播广告")
    @PostMapping("/modifySysBanner")
    @PermAnno(sort = 2, group = 13)
    public Message modifySysBanner(HttpServletRequest request, @Valid SysBannerUpdateReq sysBannerUpdateReq){
        TSysBanner tSysBanner = new TSysBanner();
        BeanUtils.copyProperties(sysBannerUpdateReq,tSysBanner);
        tSysBanner.setOperator((String) request.getSession().getAttribute(Constant.KEY_USER_USER_NAME));
        tSysBanner.setOperatorIp(request.getRemoteAddr());
        sysBannerServiceImpl.update(tSysBanner);
        return ReqCode.UpdateSuccess.getMessage();
    }

    @ApiOperation(notes = "客户端设置组", value = "查询所有轮播广告")
    @FindPagingAnno(
            items = {
                    @FindPagingItemAnno(conditions = {"=", "LIKE"}, column = "operator"),
                    @FindPagingItemAnno(column = "skipWay", fixedValue = {"0", "1"}, isString = false),
                    @FindPagingItemAnno(column = "state", fixedValue = {"0", "1","2"}, isString = false),
                    @FindPagingItemAnno(column = "type", fixedValue = {"1","2"}, isString = false),
                    @FindPagingItemAnno(conditions = {">", "<", ">=", "<=", "="}, column = "createTime", prefix = "`t_sys_banner`."),
            })
    @GetMapping("/findAllSysBanner")
    @PermAnno(sort = 3, group = 13)
    public Message findAllSysBanner(@Valid FindPagingVo findPagingVo) {
        return ReqCode.Success.getMessage(sysBannerServiceImpl.findAll(findPagingVo));
    }

}
