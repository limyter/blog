package com.jtw.sys.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:系统用户基本业务信息
 *
 * @author cjsky666
 * @date 2018/9/19 10:09
 */
@Data
public class SysUserInfoBaseVo implements Serializable {
    private static final long serialVersionUID = 619154964971716268L;
    private Long id;
    private String userName;
    private String nickName;
    private String headImage;
    private Integer sex;
    private String thumbNailImage;
    private Integer age;
    private String mobile;
    private String email;
    private String qq;
    private String address;
    private String desc;
    private Integer state;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date lastLoginTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date currentLoginTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
}
