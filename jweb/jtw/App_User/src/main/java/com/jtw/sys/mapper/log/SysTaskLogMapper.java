package com.jtw.sys.mapper.log;

import com.MyMapper;
import com.jtw.sys.model.log.TSysTaskLog;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/23 14:42
 */
public interface SysTaskLogMapper extends MyMapper<TSysTaskLog> {
}
