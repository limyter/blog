package com.jtw.sys.service.log;

import com.github.pagehelper.Page;
import com.jtw.common.baseService.BaseService;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.sys.mapper.log.SysTaskLogMapper;
import com.jtw.sys.model.log.TSysTaskLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DESCRIPT:定时任务启动记录接口
 *
 * @author cjsky666
 * @date 2018/11/23 14:33
 */
@Service
public class SysTaskLogServiceImpl implements BaseService<TSysTaskLog> {

    @Autowired
    private SysTaskLogMapper sysTaskLogMapper;
    @Override
    public void save(TSysTaskLog tSysTaskLog) {
        sysTaskLogMapper.insertUseGeneratedKeys(tSysTaskLog);
    }

    @Override
    public void saveAll(List<TSysTaskLog> list) {
        sysTaskLogMapper.insertList(list);
    }

    @Override
    public void update(TSysTaskLog tSysTaskLog) {

    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public TSysTaskLog getById(Object id) {
        return null;
    }

    @Override
    public Page<TSysTaskLog> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysTaskLog> findAll(FindPagingVo findPagingVo) {
        return null;
    }

    @Override
    public List<TSysTaskLog> getAll() {
        return null;
    }
}
