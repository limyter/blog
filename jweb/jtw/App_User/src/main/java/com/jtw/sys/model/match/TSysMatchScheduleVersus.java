package com.jtw.sys.model.match;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:赛事赛程对阵表
 *
 * @author cjsky666
 * @date 2019/7/18 10:43
 */
@Data
@Table(name = "`t_sys_match_schedule_versus`")
public class TSysMatchScheduleVersus implements Serializable {
    private static final long serialVersionUID = -866075987115565992L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Integer matchId;
    private Integer scheduleId;
    private Integer homeTeamId;
    private Integer awayTeamId;
    private Integer winTeamId;
    private Integer homeTeamOdds;
    private Integer awayTeamOdds;
    private Integer drawTeamOdds;
    private Integer homeTeamScore;
    private Integer awayTeamScore;
    private Integer betMaxBalance;
    private Integer betMinBalance;
    private Integer locked;
    private Integer sortNum;
    private Integer betType;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date startTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date endTime;
    private String operatorIp;
    private String operator;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
