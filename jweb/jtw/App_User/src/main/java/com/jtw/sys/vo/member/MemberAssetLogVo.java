package com.jtw.sys.vo.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:会员资产变更记录业务类
 *
 * @author cjsky666
 * @date 2019/1/24 15:35
 */
@Data
public class MemberAssetLogVo implements Serializable {
    private static final long serialVersionUID = 3122169186478951644L;
    private Integer id;
    private Long userId;
    private String serialNo;
    private Integer type;
    private Integer balance;
    private Integer assetBalance;
    private Integer assetFreezeBalance;
    private String descript;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
}
