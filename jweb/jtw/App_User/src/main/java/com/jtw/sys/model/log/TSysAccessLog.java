package com.jtw.sys.model.log;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jtw.common.bean.vo.ListWrapper;
import com.jtw.common.util.DeepCopyUtil;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * DESCRIPT:访问日志记录类
 *
 * @author cjsky666
 * @date 2018/10/9 16:11
 */
@Data
@Table(name = "`t_sys_access_log`")
public class TSysAccessLog implements Serializable {
    private static final long serialVersionUID = 2143420719096903182L;
    @Id
    private Long id;
    private Long userId;
    private String ip;
    private String userName;
    private String urlName;
    private String port;
    private String url;
    private String address;
    private String mac;
    private String params;
    private String httpMethod;
    private String browserType;
    private String browserName;
    private String browserVersion;
    private String os;
    private String renderEngine;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
}
