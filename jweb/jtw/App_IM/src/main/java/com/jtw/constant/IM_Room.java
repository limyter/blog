package com.jtw.constant;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.Set;

/**
 * DESCRIPT: 群聊组
 *
 * @author cjsky666
 * @date 2019/4/6 0:17
 */
@Data
public class IM_Room {
    private String groupId;//房间号,相当于绑定的群组
    private String name;//房间名称
    private String userId;//归属人,房主
    private String nickName;//归属人,名称
    private Integer maxNumber=-1;//房间最大人数限制
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;//创建时间
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date upDateTime;//更新时间
}
