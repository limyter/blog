package com.jtw.udp;

import org.tio.core.udp.UdpClient;
import org.tio.core.udp.UdpClientConf;
import org.tio.utils.SystemTimer;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/3/22 15:32
 */
public class MyUdpClientStarter {
    public static void main(String[] args) {
        UdpClientConf udpClientConf = new UdpClientConf("127.0.0.1", 3000, 5000);
        UdpClient udpClient = new UdpClient(udpClientConf);
        udpClient.start();

        long start = SystemTimer.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            String str = i + "、" + "用tio开发udp，有点意思";
            udpClient.send(str.getBytes());
        }
        long end = SystemTimer.currentTimeMillis();
        long iv = end - start;
        System.out.println("耗时:" + iv + "ms");
    }
}
