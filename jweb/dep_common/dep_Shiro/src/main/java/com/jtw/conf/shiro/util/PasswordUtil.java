package com.jtw.conf.shiro.util;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/4 13:57
 */
public class PasswordUtil {

    private static String algorithmName = "MD5";
    /**
     * 散列次数
     */
    private static final int hashIterations = 2;

    private static RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();

    public static String getSalt() {
        return randomNumberGenerator.nextBytes().toHex();
    }

    public static String encryptPassword(String password, String salt) {
        return new SimpleHash(algorithmName, password,
                ByteSource.Util.bytes(salt), hashIterations).toHex();
    }

    public static void main(String[] args) {
        System.out.println(PasswordUtil.encryptPassword("ec6a6536ca304edf844d1d248a4f08dc","74fbe15a57d63af5d0de7055ec754d9c"));
    }
}
