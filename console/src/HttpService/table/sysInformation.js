export default  {
  operator: {
    params:{
      value:"",
      column:"operator",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"操作者",
    type:1,
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"}
      ]
    }
  },
  author: {
    params:{
      value:"",
      column:"author",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"作者",
    type:1,
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"}
      ]
    }
  },
  state: {
    params:{
      value:"",
      column:"state",
      condition:"=",
      prefix:"",
      isString:false
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'等于',
    state:0,
    label:"状态",
    type:3,
    conditions:{
      options:[
        {opt:"0",alias:"待审核"},
        {opt:"1",alias:"已通过"},
        {opt:"2",alias:"已禁用"},
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "发布时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_information`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "开始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_information`.",
        },
        alias: '小于等于',
        state: 0,
        label: "结束时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  }
}

