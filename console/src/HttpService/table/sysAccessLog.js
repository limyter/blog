export default {
  ip: {
    params:{
      value:"",
      column:"ip",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"IP地址",
    type:1,//输入框类型
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"},
      ]
    }
  },
  userName: {
    params:{
      value:"",
      column:"userName",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"用户名",
    type:1,//输入框类型
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"},
      ]
    }
  },
  address: {
    params:{
      value:"",
      column:"address",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"反解析地址",
    type:1,//输入框类型
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"},
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "访问时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_access_log`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "开始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_access_log`.",
        },
        alias: '小于等于',
        state: 0,
        label: "结束时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  }
};
