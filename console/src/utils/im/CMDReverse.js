/**
 * 协议处理方法
 */

import { Notification } from 'element-ui';

//客户端的聊天框类型
const CHANNEL_TYPE={
  P2P:1,
  GROUP:2
}

export default {

  HEARTBEAT_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("心跳请求", bodyObj);
  },
  HANDSHAKE_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("握手请求", bodyObj);
  },
  HANDSHAKE_RESP:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("握手响应", bodyObj);
  },
  JOIN_GROUP_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("进入群组请求", bodyObj);
  },
  JOIN_GROUP_RESP:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("进入群组响应", bodyObj);
  },
  JOIN_GROUP_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("进入群组通知", bodyObj);
  },
  LEAVE_GROUP_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("离开群组通知", bodyObj);
  },
  P2P_CHAT_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("点对点聊天（私聊）请求", bodyObj);
  },
  P2P_CHAT_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("点对点聊天（私聊）通知", bodyObj);
    var t = store.getters.getValForKey("channels");
    var index = -1;
    for(var i=0;i<t.length;i++) {
      if (t[i].sid == (bodyObj.sid===("s"+store.getters.getUserInfo.id)?bodyObj.rid:bodyObj.sid)) {
      // if (t[i].sid == bodyObj.sid) {
        index=i;
        break;
      }
    }
    var tmp;
    if(index<0){
      tmp = {
        sid:bodyObj.sid===("s"+store.getters.getUserInfo.id)?bodyObj.rid:bodyObj.sid,
        logs:[],
        sendTime:bodyObj.sendTime,
        type:CHANNEL_TYPE.P2P,
        badge :0,
        sname:bodyObj.sname,
        state:1,//在线状态
      }
      tmp.badge++;
      tmp.logs.push(bodyObj);
      console.log("此条消息是新消息：需要在客服位置抖动一下");
      t.push(tmp);

    }else{
      if(t[index].badge==0&&bodyObj.sid!=("s"+store.getters.getUserInfo.id)&&bodyObj.sid!=store.getters.getValForKey("currentToId").sid){
        //只有未被记录过的才进行一次通知提示，且有提示音
        Notification({
          title: "来自"+bodyObj.sname+"的新消息",
          message:  (bodyObj.sname + "：" + (bodyObj.ctx.length>30?bodyObj.ctx.substring(0,30)+'...':bodyObj.ctx)),
          duration:1000
        });
        // if(recharge==null){
        //   recharge= document.getElementById("recharge");
        // }
        // if(recharge.readyState==4){
        //   recharge.play();
        // }else{
        //   console.log("音频数据还没加载完毕");
        // }
      }else{
      }
      t[index].sendTime = bodyObj.sendTime;
      t[index].logs.push(bodyObj);
      // if(bodyObj.sid!=("s"+store.getters.getUserInfo.id)&&){
      console.log(store.getters.getValForKey("currentToId"));
      if(store.getters.getValForKey("currentToId").sid!=bodyObj.sid&&bodyObj.rid!=store.getters.getValForKey("currentToId").sid){
        t[index].badge++;
      }
    }
    store.commit("saveChannels",t);
    // Vue.set(store.state,"channels",t);
    console.log(store.getters.getValForKey("channels"));
  },
  GROUP_CHAT_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("群聊请求", bodyObj);
  },
  GROUP_CHAT_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("群聊通知", bodyObj);
  },
  P2P_QUERY_CHAT_RECORD_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("获取p2p聊天记录数据-请求", bodyObj);
  },
  RUN_JS_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("运行js脚本", bodyObj);
  },
  CLOSE_PAGE:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("让客户端关闭当前页面（只作用于WEB端）", bodyObj);
  },
  MSG_TIP_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("系统通知请求", bodyObj);
  },
  MSG_TIP_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("系统通知响应", bodyObj);
    Notification({title: "来自【"+bodyObj.sname+"】的系统公告",message:bodyObj.ctx,duration:5000});
  },
  PAGE_ONLINE_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("分页获取在线观众请求", bodyObj);
  },
  PAGE_ONLINE_RESP:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("分页获取在线观众响应", bodyObj);
  },
  UPDATE_TOKEN_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("更新token", bodyObj);
  },
  UPDATE_TOKEN_RESP:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("更新token响应", bodyObj);
  },
  UNSEND_MSG_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("撤回消息", bodyObj);
  },
  UNSEND_MSG_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("撤回消息通知", bodyObj);
  },
  USER_ACTION_LOG_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("用户动作日志", bodyObj);
  },
  P2P_ALREADY_READ_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("我告诉服务器，张三发给我的私聊消息已读", bodyObj);
  },
  P2P_ALREADY_READ_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("服务器告诉张三，张三发给李四的私聊，李四已经阅读（暂不实现，因为此需求有争议）", bodyObj);
  },
  P2P_NOT_READ_COUNT_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("查询未读私聊消息数请求", bodyObj);
  },
  P2P_NOT_READ_COUNT_RESP:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("查询未读私聊消息数响应", bodyObj);
  },
  ALL_CHAT_ROMM_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("获取客服列表指令", bodyObj);
  },
  ALL_CHAT_ROMM_RESP:function (ws, event, commandName, bodyStr, bodyObj,store) {
    store.state.currentToId = bodyObj.ctx;
  },
  LEAVE_GROUP_REQ:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("离开群组请求", bodyObj);
  },
  All_ONLINE_RESP:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("在线用户列表响应", bodyObj.ctx);
    store.commit("saveChannels",bodyObj);
  },
  USER_ONLINE_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("用户上线通知", bodyObj);
    var t = store.getters.getValForKey("channels");
    console.log(t);
    var index = -1;
    for(var i = 0;i<t.length;i++){
      if(t[i].sid == bodyObj.sid){
        Vue.set(t[i],"state",bodyObj.state);
        // t[i].state=bodyObj.ctx;
        console.log("====================");
        console.log(t[i]);
        index = i;
        break;
      }
    };

    if(index<0){
      t.unshift(bodyObj);
    }
    store.commit("saveChannels",t);
  },
  USER_OFFLINE_NTY:function (ws, event, commandName, bodyStr, bodyObj,store) {
    console.log("用户断线通知", bodyObj);
    var t = store.getters.getValForKey("channels");
    for(var i = 0;i<t.length;i++){
      if(t[i].sid == bodyObj.sid){
        t[i].state=bodyObj.state;
        store.commit("saveChannels",t);
        break;
      }
    }
  },
};
