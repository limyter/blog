/**
 * 通信内容包装类--- 服务端
 * @param toId 接收者
 * @param fromId 发送者
 * @param ctx 主要内容Object
 * @constructor
 */
function IMMessage(rid,sid,ctx,rname,sname){
  this.rid= rid;
  this.sid= "s"+sid;
  this.ctx= ctx;
  this.rname= rname;
  this.sname= sname;
}

IMMessage.prototype = {
  constructor:IMMessage
}

export default IMMessage;
